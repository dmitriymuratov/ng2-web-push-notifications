import {Component, OnDestroy, OnInit} from '@angular/core';
import {PushNotificationsService} from 'ng-push';
import {ApiService} from './services/api.service';
import 'rxjs/add/operator/take';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/takeWhile';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {

  title = 'Web push Notifications!';
  data: any;
  private cityId = 706483;
  sub: Subscription;
  getApiTimeout: true;

  constructor(private _pushNotifications: PushNotificationsService,
              private _apiService: ApiService) {
    _pushNotifications.requestPermission(); // request for permission as soon as component loads
  }

  // our function to be called on click
  notify() {

    this._apiService.getNewData(this.cityId).take(1).subscribe(
      res => {
        this.data = res;

        const time = this.getTime(this.data.dt);
        const date = new Date().toLocaleDateString();

        const title = this.data.name + ' Weather ' + date + ' - ' + time;
        const options = { // set options
          body: 'Now is ' + this.data.main.temp + 'C, with ' + this.data.main.humidity + '% humidity',
          icon: '//openweathermap.org/img/w/' + this.data.weather[0].icon + '.png' // adding an icon
        };
        const notify = this._pushNotifications.create(title, options).subscribe( // creates a notification
          sc => console.log(sc),
          err => console.log(err)
        );

      },
      error => {
        console.log(error);
      }
    );

  }

  ngOnInit(): void {

    this._apiService.getNewData(this.cityId).subscribe(
      res => {
        this.data = res;
      },
      error => {
        console.log(error);
      }
    );

    this.sub = Observable.interval(15 * 60000)
      .takeWhile(() => !this.getApiTimeout)
      .subscribe(i => {
        // This will be called until `getApiTimeout` flag is set to true
        this.notify();
      });

  }

  private getTime(value) {
    const date = new Date(value * 1000);
    const hours = date.getHours();
    const minutes = '0' + date.getMinutes();
    const seconds = '0' + date.getSeconds();
    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
